<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <p>First Name:</p>
        <input type="text" name="firstName">
        <p>Last Name:</p>
        <input type="text" name="lastName">
        <p>Gender:</p>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br>
        <p>Nationality:</p>
        <select name="nationality">
            <option value="ID">Indonesia</option>
            <option value="SG">Singapura</option>
            <option value="PH">Philipina</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>
        <p>Bio:</p>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>